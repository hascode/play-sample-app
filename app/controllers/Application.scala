package controllers

import org.joda.time.LocalTime
import models.com.hascode.Item
import play.api._
import play.api.mvc._
import play.api.libs.json.JsObject
import play.api.libs.json.JsString

object Application extends Controller {

  def index = Action {
    Ok(views.html.index("It works!", Seq(Item("tim", LocalTime.now(), "hey there"), Item("ted", LocalTime.now(), "whats up.."))))
  }

  def jsonData = Action {
    Ok(JsObject(Seq("1" -> JsString("fred"), "2" -> JsString("selma"), "3" -> JsString("fooo"))))
  }
}